#!/usr/bin/env python
from sys import argv

'''
@author: Samin Farhangi; 11-2-2022

This script takes a filtered summary, a filtered sigHB file, and a pig bed file as input.
It then annotates the eQTLs from the summary based on the information from the other two files.
The output is written to a file named <input_file>_annotate_eqtl.txt.
'''

def summary_file (summary):
    """
    Parses the summary file into a list of records.

    :param summary: A file object representing the summary file.
    :return: A list of records from the summary file.
    """
    summary_list =[]
    for line in summary:
        if line.startswith("ENSS"):
            sum_line = line.strip().split()
            gene_id = sum_line[0]
            chr_snp = sum_line[1]
            N_snp = sum_line[2]
            snp_id_most_sig = sum_line[3]
            start_eqtl = sum_line [4]
            stop_eqtl = sum_line[5]
            qvalue_most_sig = sum_line[8]
            eqtl_id = chr_snp+"_"+start_eqtl+"_"+stop_eqtl
            summary_list.append([eqtl_id, chr_snp, start_eqtl, stop_eqtl, gene_id, snp_id_most_sig, N_snp, qvalue_most_sig])
    return summary_list

def get_pos_snp (signHB_filtered_file):
    """
    Parses the filtered sigHB file into a dictionary mapping SNP IDs to their positions.

    :param signHB_filtered_file: A file object representing the filtered sigHB file.
    :return: A dictionary mapping SNP IDs to positions.
    """
    pos_snp_dict = {}
    for line in signHB_filtered_file:
        pos_snp_line = line.strip().split()
        pos_snp = pos_snp_line[4]
        snp_id = pos_snp_line[5]
        pos_snp_dict[snp_id] = pos_snp
    return pos_snp_dict

def parse_bed_pig(bed_file):
    """
    Parses the pig bed file into a dictionary mapping gene IDs to their attributes.

    :param bed_file: A file object representing the pig bed file.
    :return: A dictionary mapping gene IDs to tuples of their attributes.
    """
    bed_dict = {}
    for line in bed_file:
        if not line.startswith("chromosome_name"):
            gene_id_line = line.strip().split()
            chr_gene = gene_id_line[0]
            start_gene = gene_id_line[1]
            end_gene = gene_id_line[2]
            strand_gene = gene_id_line[3]
            ensembl_gene_id = gene_id_line[4]
            gene_biotype = gene_id_line[5]
            gene_name = gene_id_line[6]

            bed_dict[ensembl_gene_id] = (chr_gene, start_gene, end_gene, strand_gene, gene_biotype, gene_name)

    return bed_dict

def annot_eqtl(summary_list, bed_dict, pos_snp_dict):
    """
    Annotates eQTLs based on the summary, bed, and pos_snp information.

    :param summary_list: A list of records from the summary file.
    :param bed_dict: A dictionary mapping gene IDs to tuples of their attributes.
    :param pos_snp_dict: A dictionary mapping SNP IDs to positions.
    :return: A list of annotated eQTLs.
    """
    eqtl_annot=[]
    for eqtl in summary_list:
        eqtl_id = eqtl[0]
        chr_snp = eqtl[1]
        start_eqtl = eqtl[2]
        stop_eqtl = eqtl[3]
        gene_id = eqtl[4]
        snp_id_most_sig = eqtl[5]
        N_snp = eqtl[6]
        qvalue_most_sig = eqtl[7]

        if gene_id in bed_dict.keys():
            if snp_id_most_sig in pos_snp_dict.keys():
                eqtl_annot.append([eqtl_id,chr_snp, start_eqtl, stop_eqtl,snp_id_most_sig, pos_snp_dict[snp_id_most_sig],
                                   N_snp, qvalue_most_sig, gene_id, bed_dict[gene_id][0], bed_dict[gene_id][1],
                                   bed_dict[gene_id][2], bed_dict[gene_id][3], bed_dict[gene_id][4], bed_dict[gene_id][5]])
    return eqtl_annot

def update_annot(eqtl_annot):
    """
    Updates the annotated eQTLs with distance information and whether the eQTL is cis or trans.

    :param eqtl_annot: A list of annotated eQTLs.
    :return: A list of updated annotated eQTLs.
    """
    update_annot_eqtl =[]
    for eqtl in eqtl_annot:
        chr_snp = eqtl[1]
        chr_gene = eqtl[9]
        pos_snp = int(eqtl[5])
        start_gene = int(eqtl[10])
        stop_gene = int(eqtl[11])
        if chr_snp == chr_gene:
            dis_start = abs(pos_snp - start_gene)
            if dis_start <= 1000000 and dis_start >= 0:
                update_annot_eqtl.append([eqtl[0], eqtl[1], eqtl[2], eqtl[3], eqtl[4], eqtl[5], eqtl[6], eqtl[7], eqtl[8],
                                 eqtl[9], eqtl[10], eqtl[11], eqtl[12], eqtl[13], eqtl[14], "cis_eQTL", dis_start])
            else:
                update_annot_eqtl.append([eqtl[0], eqtl[1], eqtl[2], eqtl[3], eqtl[4], eqtl[5], eqtl[6], eqtl[7],
                                     eqtl[8], eqtl[9], eqtl[10], eqtl[11], eqtl[12], eqtl[13], eqtl[14], "trans_eQTLI", dis_start])
        else:
            update_annot_eqtl.append([eqtl[0], eqtl[1], eqtl[2], eqtl[3], eqtl[4], eqtl[5], eqtl[6],
                                             eqtl[7],eqtl[8], eqtl[9], eqtl[10], eqtl[11], eqtl[12], eqtl[13], eqtl[14],
                                             "trans_eQTLII", "-"])
    return update_annot_eqtl

def write_output(update_annot_eqtl, outname1):
    """
    Writes the updated annotated eQTLs to an output file.

    :param update_annot_eqtl: A list of updated annotated eQTLs.
    :param outname1: The name of the output file.
    """
    out1 = open(outname1, 'w')
    out1.write('eqtl_id\tchr_snp\tstart_eqtl\tstop_eqtl\tsnp_id_most_sig\tsnp_most_sig_pos\t'
               'N_snp\tqvalue_most_sig\tgene_id\tchr_gene\tstart_gene\tend_gene\tstrand_gene\tgene_biotype\t'
               'gene_name\tcis_or_trans\tdistance_SNP_start\n')
    n=1
    for record in update_annot_eqtl:
        out1.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%(n,record[0], record[1], record[2],record[3], record[4], record[5], record[6],record[7], record[8], record[9], record[10],record[11], record[12], record[13],record[14],record[15], record[16]))
        n = n+1
    out1.close()

if __name__ == "__main__":
    # Open the input files
    infile_name_1 = argv[1] #summary_filter.txt
    infile_1 = open(infile_name_1, 'r')

    infile_name_2 = argv[2]  # filtered-signHB.txt
    infile_2 = open(infile_name_2, 'r')

    infile_name_3 = "Sus_scrofa.Sscrofa11.1.104.bed"
    infile_3 = open(infile_name_3, 'r')

    # Call the functions to parse the files and annotate the eQTLs
    summary_list = summary_file(infile_1)
    pos_snp_dict = get_pos_snp(infile_2)
    bed_dict = parse_bed_pig(infile_3)
    eqtl_annot = annot_eqtl(summary_list, bed_dict, pos_snp_dict)
    update_annot_eqtl = update_annot(eqtl_annot)

    # Write the output
    out_name_1 = infile_name_1.split("_")[0] + "_annotate_eqtl.txt"
    write_output(update_annot_eqtl, out_name_1)

    # Close the input files
    infile_1.close()
    infile_2.close()
    infile_3.close()
