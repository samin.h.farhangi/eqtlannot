#!/usr/bin/env bash

# eQTLannot Pipeline: Annotate and filter the result of eGWAS resulted from GCTA

# Usage: bash eQTLannot.sh
#
# Requirements: Python 3
#
# Note: This script assumes the presence of three input files:
# 'signBH_genes.txt' (contains all significant associations from eGWAS) and
# 'summary' (contains eQTL regions by merging the associations with length up to 10Mb).
#'Sus_scrofa.Sscrofa11.1.104.gtf' (the GTF file of the pig genome)

# It also requires GTF_to_bed.py, filter_summary.py, annotate_bed.py, and filter_signbhN2.py Python scripts.

echo "1-Preparing the reference genome..."
# Convert GTF to BED format
python GTF_to_bed.py Sus_scrofa.Sscrofa11.1.104.gtf
echo "Output is: Sus_scrofa.Sscrofa11.1.104.bed..."
# Count the number of protein coding genes in BED file
echo "The number of protein coding gene in Sus_scrofa.Sscrofa11.1.104.bed : $(grep -c "protein_coding" Sus_scrofa.Sscrofa11.1.104.bed)"

echo "2-Filtering the SNPs in summary..."
# Filter the SNPs in summary based on "N" >= 2 which is the number of SNPs per each eQTL regions
python filter_summary.py summary
echo "Output is: summary_filter.txt..."
# Count associations before and after filtering
echo "The number of significant associations BEFORE filtering : $(awk '{ sum += $3 } END { print sum }' summary)"
echo "The number of significant associations AFTER filtering : $(awk '{ sum += $3 } END { print sum }' summary_filter.txt)"

echo "3-Creating an input including unique target genes (ensemble ID) in summary_filter.txt..."
# Create an input including unique target genes (ensemble ID) in summary_filter.txt
awk 'NR > 1 {print $1}' summary_filter.txt | sort | uniq > summary_filter_gene.txt
echo "Output is: summary_filter_gene.txt..."

echo "4- Annotating summary_filter_gene.txt..."
# Annotate summary_filter_gene.txt
python annotate_bed.py summary_filter_gene.txt
echo "Output is: summary_filter_gene_annotate.bed..."
# Count the number of protein coding genes in annotated BED file
echo "The number of protein coding gene in summary_filtered_gene_annotate.bed : $(grep -c "protein_coding" summary_filter_gene_annotate.bed)"

echo "5-Filtering sigHB_genes.txt..."
# Filter sigHB_genes.txt including all associations based on summary files to remove association related to the regions which had 1 or 2 SNPs
python filter_signbhN2.py summary signBH_genes.txt
echo "Output is:  filtered_signBH_genes.txt..."

echo "6-Annotating the eQTLs in summary_filter..."
# Annotate the eQTLs in summary_filter, clustering them to cis and trans-I and -II
python ./summary_annotate.py summary_filter.txt filtered_signBH_genes.txt
echo "Output is: summary_annotate_eqtl.txt..."
