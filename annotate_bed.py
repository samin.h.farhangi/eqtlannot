#!/usr/bin/env python
from sys import argv

"""
@author: Samin Farhangi; 07-04-2022

This program takes a bed file for a pig and identifies the biotype of each gene based on the bed file.
It's designed to work with an annotation bed file for a pig and a list of genes in a txt file.


Inputs: 
1) A txt file including Ensembl ID of genes
2) Annotation bed file for a pig 

Output: 
A bed file that includes gene name, start, end, biotype, and gene name.

"""

def parse_bed_file(bed_file):
    """
    Parses the bed file and returns a dictionary.
    The keys are ensembl_gene_ids and the values are tuples containing gene details.
    """
    bed_dict = {}
    for line in bed_file:
        if not line.startswith("chromosome_name"):
            gene_id_line = line.strip().split()
            chromosome_name = gene_id_line[0]
            start_position  = gene_id_line[1]
            end_position = gene_id_line[2]
            strand  = gene_id_line[3]
            ensembl_gene_id  = gene_id_line[4]
            gene_biotype  = gene_id_line[5]
            gene_name = gene_id_line[6]
            bed_dict[ensembl_gene_id] = (chromosome_name, start_position, end_position, strand, gene_biotype, gene_name)
    return bed_dict


def write_output(bed_dict, gene_file, outname):
    """
    Writes the output to a bed file.
    Includes gene name, start, end, biotype, and gene name.
    """
    with open(outname, 'w') as out1:
        out1.write('chromosome_name\tstart_position\tend_position\tstrand\tensembl_gene_id\tgene_biotype\texternal_gene_name\n')
        for line in gene_file:
            gene_id = line.strip().split()[0]
            if gene_id in bed_dict.keys():
                out1.write('\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (
                bed_dict[gene_id][0], bed_dict[gene_id][1], bed_dict[gene_id][2], bed_dict[gene_id][3], gene_id,
                bed_dict[gene_id][4], bed_dict[gene_id][5]))


if __name__ == "__main__":
    infile_name1 = argv[1]
    with open(infile_name1, 'r') as infile1, open("Sus_scrofa.Sscrofa11.1.104.bed", 'r') as infile2:
        bed_dict = parse_bed_file(infile2)
        out_name = infile_name1[:-4] + "_annotate.bed"
        write_output(bed_dict, infile1, out_name)
