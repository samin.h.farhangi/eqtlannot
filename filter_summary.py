from sys import argv


"""
@author: Samin Farhangi; 07-04-2022

This program takes the summary file resulted from eGWAS analysis and removed records with N = 1 or 2

"""

def write_output(summary_file, outname):
    """
    Reads a summary file line by line and writes the line to an output file if the second field of the line
    (representing the number of SNPs) is either 1 or 2. The function assumes that the input file has lines starting with "ENSSS".

    :param summary_file: A file object representing the summary file
    :param outname: The name of the output file
    """

    with open(outname, 'w') as out:
        # Writing header line to the output file
        out.write('CHR\tN\tSNP\tSTART\tSTOP\tP\tlastP\tq\tBonf\tBeta\tes\tA1\tfreq\n')

        for line in summary_file:
            # Splitting the line into fields
            rec = line.strip().split()
            # Checking if the line starts with "ENSSS" and the SNP count (second field) is 1 or 2
            if line.startswith("ENSSS") and rec[2] not in ["1", "2"]:
                out.write(line)


if __name__ == "__main__":
    # Get the name of the input file from command line
    infile_name = argv[1]

    # Constructing the name of the output file by appending "_filter.txt" to the  name of the input file
    out_name = infile_name + "_filter.txt"

    with open(infile_name, 'r') as infile:
        write_output(infile, out_name)
