#!/usr/bin/env python
import sys

"""
@author: Samin Farhangi; 07-04-2022

This program takes a GTF file for a pig and creat a bed file
"""

def parse_gtf_file(gtf_file):
    """
    This function reads a GTF file line by line and extracts relevant information.
    It stores this information in a dictionary where the keys are gene_ids and the values are tuples of relevant gene information.

    :param gtf_file: A file object representing the GTF file
    :return: A dictionary of gene information
    """

    bed_dict = {}
    for line in gtf_file:
        if not line.startswith("#"):
            gene_id = line.strip().split("gene_id")[1].split('"')[1]
            gene_biotype = line.strip().split("gene_biotype")[1].split('"')[1]

            chr = line.strip().split()[0]
            gene_type = line.strip().split()[2]
            start = line.strip().split()[3]
            stop = line.strip().split()[4]
            strand = line.strip().split()[6]

            if gene_type == "gene":
                if "gene_name" in line:
                    gene_name = line.strip().split("gene_name")[1].split('"')[1]
                    bed_dict[gene_id] = (chr, start, stop, strand, gene_biotype, gene_name)
                else:
                    bed_dict[gene_id] = (chr, start, stop, strand, gene_biotype, "-")

    return bed_dict

def write_output(bed_dict, outname):
    """
    This function writes the gene information from the dictionary to a file in BED format.

    :param bed_dict: A dictionary of gene information
    :param outname: The name of the output file
    """

    with open(outname, 'w') as out:
        out.write('chromosome_name\tstart_position\tend_position\tstrand\tensembl_gene_id\tgene_biotype\texternal_gene_name\n')
        for gene_id in bed_dict.keys():
            out.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (
            bed_dict[gene_id][0], bed_dict[gene_id][1], bed_dict[gene_id][2], bed_dict[gene_id][3], gene_id,
            bed_dict[gene_id][4], bed_dict[gene_id][5]))


if __name__ == "__main__":
    # Get the name of the input file from command line
    infile_name1 = sys.argv[1]

    # Open the GTF file and parse it
    with open(infile_name1, 'r') as infile:
        bed_dict = parse_gtf_file(infile)

    # Write the parsed information to a BED file
    out_name = infile_name1.split(".gtf")[0] + ".bed"
    write_output(bed_dict, out_name)




