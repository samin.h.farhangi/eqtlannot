#!/usr/bin/env python
from sys import argv

'''
@author: Samin Farhangi; 10-07-2022

This program processes signHB_genes.txt files (including all associations resulting from eGWAS) and removes associations which have been filtered according to N.
Output: a new signHB file after filtering based on N.
'''

def parse_summary(summary_file):
    """
    Parses the summary file and returns a list of genes and associations with N <= 2.
    """
    s_list = []
    for line in summary_file:
        if line.startswith("ENSS"):
            s_line = line.strip().split()
            gene_id = s_line[0]
            N = int(s_line[2])
            snp_id = s_line[3]
            start_snp = s_line[4]
            stop_snp = s_line[5]
            if N <=2:
                s_list.append([N, snp_id, start_snp, stop_snp, gene_id])
    return s_list


def write_output(s_list, signHB_file, outname):
    """
    Writes to the output file, filtering out lines that are in the s_list.
    """
    with open(outname, 'w') as out1:
        for line in signHB_file:
            signHB_line = line.strip().split()
            snp_pos = signHB_line[4]
            snp_id_sign = signHB_line[5]
            gene_id_sign = signHB_line[10]

            if not any(gene_id_sign == item[4] and (snp_pos == item[2] or snp_pos == item[3]) for item in s_list):
                out1.write(line)


if __name__=="__main__":
    infile_name_1 = argv[1]
    infile_name_2 = argv[2]
    out_name = "filtered_" + infile_name_2

    with open(infile_name_1, 'r') as infile_1, open(infile_name_2, 'r') as infile_2:
        s_list = parse_summary(infile_1)
        write_output(s_list, infile_2, out_name)
