# eQTLannot: eQTL Annotator

`eQTLannot` is designed to annotate and filter the results of an expression genome-wide association study (eGWAS) that has been conducted using GCTA software. It processes the summary of eGWAS results and a list of significant genes to identify, annotate, and count eQTLs (expression quantitative trait loci) - genetic variants that are associated with variations in gene expression levels in a given tissue or cell.

## Usage

Clone the repository and navigate to the eQTLannot directory:

```bash
git clone git@gitlab.com:samin.h.farhangi/eqtlannot.git
cd eqtlannot
```

You can use the script with the following command:

```bash
bash eQTLannot.sh > eQTLannot_results.log 2>&1
```

The steps of the process are explained in the `eQTLannot_results.log` file.

### Inputs

The following inputs should be in the same directory as the bash file.

- `summary`: A summary file with the following columns: `eqtl_id, chr_snp, start_eqtl, stop_eqtl, gene_id, snp_id_most_sig, N_snp, qvalue_most_sig`. The file should be space-delimited.

- `signBH_genes.txt`: This file includes all significant associations from eGWAS.

- `Sus_scrofa.Sscrofa11.1.104.gtf`: This is a pig GTF file.

Copy your the files into the eQTLannot directory. For instance:

```bash
cp ./test/* ./
```

### Outputs

The results of the script are saved in `eQTLannot_results.log`.

## Requirements

This script requires Python 3.

## Author

Samin Farhangi


